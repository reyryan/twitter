<?php
require_once('TwitterAPIExchange.php');

class Twitter {

	private $settings = array(
		'oauth_access_token' => "631765457-Gl1I0SiZvuapYmBP2VHAX7YC8jKhc9On361aNvJ3",
		'oauth_access_token_secret' => "0EfqbI2ZZ0uDMfqKl6t3ZqUyxW4rR5p4H0tj8EGOFnJco",
		'consumer_key' => "G3BPU4WpGd9sMF54DGdV22FWl",
		'consumer_secret' => "SjqPOVYTU13GXu8dcRzU7u0sX2WyHBxcC0icCfKHwOjIh6wfk0"
	);
	
	protected $oTwitter = null;
	
	protected $requestMethod = 'GET';
	
	protected $username;
	
	protected $totalTweets = 0;
	protected $followingCount = 0;
	protected $followersCount = 0;
	
	public function __construct($username, $reqMethod = 'GET') {
		$this->requestMethod = $reqMethod;
		$this->username = $username;
		
		$this->oTwitter = new TwitterAPIExchange($this->settings);
	}
	
	/** Retrieve the recent tweets by a username
	*/
	public function getRecentTweets($num = 5) {
		$url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
		$getfield = "?screen_name=" . $this->username;
		
		$data = json_decode($this->oTwitter->setGetfield($getfield)->buildOauth($url, $this->requestMethod)->performRequest(),$assoc = TRUE);
		
		$this->totalTweets = count($data);
		echo "Recent $num tweets<hr/>";
		foreach($data as $items)
		{
			if($num-- <= 0) break;
			echo "Time and Date of Tweet: ".$items['created_at']."<br />";
			echo "Tweet: ". $items['text']."<br /><br />";
		}
		echo "<hr/>";
	}
	
	/**
	* Count the total number of tweets
	*/
	public function getTotalTweets() {
		return $this->totalTweets;
	}
	
	public function getFollowing() {
		$url = "https://api.twitter.com/1.1/friends/list.json";
		$getfield = "?skip_status=true&include_user_entities=false&screen_name=" . $this->username;
		
		$data = json_decode($this->oTwitter->setGetfield($getfield)->buildOauth($url, $this->requestMethod)->performRequest(),$assoc = TRUE);
		
		echo "Following: " . count($data['users']) . "<hr/>";
		foreach($data['users'] as $user) {
			$following = $user['screen_name'];
			echo "$following<br/>";
		}
		echo "<hr/>";
	}
	
	public function getFollowers() {
		$url = "https://api.twitter.com/1.1/followers/list.json";
		$getfield = "?skip_status=true&include_user_entities=false&screen_name=" . $this->username;
		
		$data = json_decode($this->oTwitter->setGetfield($getfield)->buildOauth($url, $this->requestMethod)->performRequest(),$assoc = TRUE);
		
		echo "Followers: " . count($data['users']) . "<hr/>";
		foreach($data['users'] as $user) {
			$follower = $user['screen_name'];
			echo "$follower<br/>";
		}
		echo "<hr/>";
	}
}